import 'package:expense_app/widgets/new_transaction.dart';

import 'package:flutter/material.dart';

import './models/transaction.dart';
import './widgets/transaction_list.dart';

void main() => runApp(ExpenseApp());

class ExpenseApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Expense Manager',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  // String titleInput;
  // String amountInput;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  final List<Transaction> _userTransaction = [
    Transaction(id: 't1', title: 'Shirt', amount: 199, date: DateTime.now()),
    Transaction(id: 't2', title: 'Shoes', amount: 80, date: DateTime.now()),
    Transaction(id: 't3', title: 'Laptop', amount: 1199, date: DateTime.now()),
    Transaction(id: 't4', title: 'Petrol', amount: 20, date: DateTime.now())
  ];

  void _addUserTransaction(String title, double amount) {
    final newTransaction = Transaction(
      title: title,
      amount: amount,
      date: DateTime.now(),
      id: DateTime.now().toString(),
    );

    setState(() {
      _userTransaction.add(newTransaction);
    });
  }

  void _startAddUserExpense(ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return NewTransaction(
            transactionHandler: _addUserTransaction,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Expense Manager'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () => _startAddUserExpense(context),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Card(
                  elevation: 3,
                  child: Text(
                    'Hello World',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 30, color: Colors.grey),
                  ),
                ),
              ),
              TransactionList(userTransaction: _userTransaction)
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => _startAddUserExpense(context),
        ),
      );
  }
}
