import 'package:flutter/material.dart';

class NewTransaction extends StatefulWidget {
  final Function transactionHandler; 

  NewTransaction({this.transactionHandler});

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleController = TextEditingController();

  final amountController = TextEditingController();

  void addTransaction(){
    final title = titleController.text;
    final amount =  double.parse(amountController.text);
    if(title.isEmpty || amount <=0){
      return;
    }
    widget.transactionHandler(title, amount);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: 'Title'),
              controller: titleController,
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Amount'),
              controller: amountController,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              onSubmitted: (_)=>addTransaction() ,
            ),
            FlatButton(
              child: Text(
                'Add Transaction',
                style: TextStyle(color: Colors.blue),
              ),
              onPressed: ()=> addTransaction(),
            )
          ],
        ),
      ),
    );
  }
}
